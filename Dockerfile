FROM base/archlinux

COPY . /csl

RUN cd /csl && ./install && mkdir -p /testcsl/{bin,conf,lib} && cp -v /csl/csl-test /testcsl/bin/csl-test && mkdir -p /root/testcsl/{bin,conf,lib} && cp -v /csl/csl-test /root/testcsl/bin/csl-test && mkdir -p /usr/{bin,lib}/orbos /etc/orbos && cp -v /csl/csl-test /usr/bin/orbos

RUN printf '#!%s\n\n%s\n\n%s\n%s\n\n%s\n%s\n%s\n\n%s\n%s\n%s\n\n%s\n%s\n%s\n%s\n\n' "/usr/bin/env bash" "echo; echo \"Running 4 potential test locations for scripts using CSL.\"" "echo; echo \"Running test for standard bin locations.\"; echo;" "csl-test" "echo; echo \"Running test for \\\$HOME locations.\"; echo;" "cd /root/testcsl/bin" "./csl-test" "echo; echo \"Running test for orbos locations.\"; echo;" "cd /usr/bin/orbos" "./csl-test" "echo; echo \"Running test for other locations.\"; echo;" "cd /testcsl/bin/" "./csl-test" "echo;" > /test-all-csl && chmod a+x /test-all-csl

CMD ["/test-all-csl"]
